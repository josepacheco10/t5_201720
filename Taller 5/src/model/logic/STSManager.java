package model.logic;

import java.util.Comparator;
import java.util.Iterator;

import Sorting.HeapSort;
import api.ISTSManager;
import model.data_structures.DoubleLinkedList;
import model.data_structures.IList;
import model.data_structures.MaxPQ;
import model.vo.ResumenList;
import model.vo.RutasVO;
import model.vo.TripsVO;

public class STSManager implements ISTSManager {

	// -------------------------------------
	// Constantes.
	// -------------------------------------
	public static final String TRIPS_FILE = "data/trips.txt";
	
	// -------------------------------------
	// Atributos.
	// -------------------------------------
	
	/**
	 * Clase que contiene los m�todos de lectura
	 */
	private static ReaderClass central;
	
	/**
	 * Atributo que representa la estructura de una Lista.
	 */
	private static IList<TripsVO> viajes;
	
	/**
	 * Atributo que representa la estructura de una Lista.
	 */
	private static IList<ResumenList> resumenList;
	
	// -------------------------------------
	// Constructor.
	// -------------------------------------
	public STSManager() {
		central = new ReaderClass();
		viajes = new DoubleLinkedList<TripsVO>();
		resumenList = new DoubleLinkedList<ResumenList>();
	}
	
	// -------------------------------------
	// M�todos.
	// -------------------------------------
	
	/**
	 * M�todo encargado de realizar la lectura de un archivo CSV con la
	 * informacion de los viajes.
	 */
	public void loadTrips() {
		central.loadTrips(TRIPS_FILE);
		viajes = central.loadTrips(TRIPS_FILE);
	}

	/**
	 * M�todo encargado de ordenar la Lista de viajes por el par�metro: BlockId.
	 */
	public void ordenarTrips()
	{
		viajes = viajes.mergeSort(viajes, new Comparator<TripsVO>() {
			public int compare(TripsVO arg0, TripsVO arg1) {
				return arg0.getBlockId() - arg1.getBlockId();
			}
		});
	}
	
	/**
	 * A partir de la Lista de Viajes, obtiene un resumen con determinados par�metros.
	 */
	public void buildResumenList()
	{		
		resumenList = new DoubleLinkedList<ResumenList>();
		IList<Integer> aux = new DoubleLinkedList<Integer>();

				
		// Iterador Lista
		Iterator<TripsVO> iterTrips = viajes.iterator();
		int a = 0;
		
		int j = 1;
		boolean stop = false;
		int contador = 1;
		
		while(iterTrips.hasNext() && !stop)
		{
			TripsVO viajeObj = viajes.getElement(a);
			TripsVO viajeObjDos = viajes.getElement(j);
			
			if(viajeObj.getBlockId() == viajeObjDos.getBlockId())
			{
				aux.addLast(viajeObj.getTripId());
				contador++;
			}
				
			else
			{
				ResumenList resumen = new ResumenList(viajeObj.getBlockId(), viajeObj.getRouteId(), contador, aux);
				resumenList.addLast(resumen);
				contador = 1;
				aux = new DoubleLinkedList<Integer>();
			}
			
			// Avances
			iterTrips.next();
			a++;
			j++;
			
			if(j == viajes.getSize())
			{
				a++;
			}
			
			if(a == viajes.getSize())
			{
				ResumenList resumen = new ResumenList(viajeObj.getBlockId(), viajeObj.getRouteId(), contador, aux);
				resumenList.addLast(resumen);
				stop = true;
			}
		}
	}
	
	/**
	 * Ordena la Lista de identificadores de viaje a trav�s del algoritmo HeapSort
	 */
	public void ordenarHeapSort()
	{
		Iterator<ResumenList> iter = resumenList.iterator();
		int a = 0;
		
		while(iter.hasNext())
		{
			ResumenList resumen = resumenList.getElement(a);
			IList<Integer> trips = resumen.getTripsID();
			int arr[] = new int[trips.getSize()];
			
			for(int i = 0; i < arr.length; i++)
			{
				arr[i] = trips.getElement(i);
			}
			
			HeapSort ordenar = new HeapSort();
			ordenar.sort(arr);
			
			iter.next();
			a++;
		}
	}
	
	public void ordenarMergeDos()
	{
		viajes = viajes.mergeSort(viajes, new Comparator<TripsVO>() {
			public int compare(TripsVO arg0, TripsVO arg1) {
				return arg0.getRouteId() - arg1.getRouteId();
			}
		});
	}
	
	/**
	 * M�todo para obtener los identificadores de Ruta y su frecuencia en la lista resumen.
	 * @return IList. Lista de tipo RutasVO --> Atributos: RouteID, Frecuencia(cantidad).
	 */
	public IList<RutasVO> obtenerRutas()
	{
		ordenarMergeDos();
		IList<RutasVO> retorno = new DoubleLinkedList<RutasVO>();

		Iterator<TripsVO> iter = viajes.iterator();
		
		int a = 0;
		int b = 1;
		
		int contador = 1;
		boolean stop = false;

		while (iter.hasNext() && !stop) {
			
			TripsVO uno = viajes.getElement(a);
			TripsVO dos = viajes.getElement(b);

			if (uno.getRouteId() == dos.getRouteId()) {
				contador++;
			} 
			else 
			{
				RutasVO ruta = new RutasVO(uno.getRouteId(), contador);
				retorno.addLast(ruta);
				contador = 1;
			}

			// Avances
			iter.next();
			a++;
			b++;
			
			// Si el avance en b (uno mayor a A), se aumenta en uno el valor de A.
			if (b == viajes.getSize()) {
				a++;
			}

			// Cuando el valor de A es igual al tama�o de Lista, se agrega el �ltimo elemento sin comparaciones.
			if (a == viajes.getSize()) {
				
				RutasVO ruta = new RutasVO(uno.getRouteId(), contador);
				retorno.addLast(ruta);
				stop = true;
			}
		}
		return retorno;
	}
	
	/**
	 * Obtiene un arreglo de los valores de las frecuencias en la lista de rutas.
	 * @return int[]. Arreglo de tipo Integer.
	 */
	public int[] frecuenciaArreglo()
	{
		IList<RutasVO> routes = obtenerRutas();
		
		Iterator<RutasVO> iter = routes.iterator();
		int[] retorno = new int[routes.getSize()];
		int a = 0;
		
		while(iter.hasNext())
		{
			RutasVO obj = routes.getElement(a);
			retorno[a] = obj.getCantidad();
			
			// Avance
			iter.next();
			a++;
		}
		
		return retorno;
	}	
	
	/**
	 * M�todo para agregar las rutas a una Priority Queue, a partir de esta se saca el m�ximo
	 * valor y se agrega a una lista.
	 * @return IList. Lista con las Rutas y su prioridad ordenadas. Mayor a menor.
	 */
	public IList<RutasVO> listasPQ()
	{
		// Lista de rutas (sin ID repetidos).
		IList<RutasVO> routes = obtenerRutas();
		
		// Lista de retorno
		IList<RutasVO> retorno = new DoubleLinkedList<RutasVO>();
		
		// Uso de la Cola de Prioridad
		int[] frecuencia = frecuenciaArreglo();
		MaxPQ<Integer> pQ = new MaxPQ<Integer>(frecuencia.length);
		
		// Insert
		for(Integer frec: frecuencia)
		{
			pQ.insert(frec);
		}
		
		// Lista auxiliar con las frecuencias ordenadas
		IList<Integer> aux = new DoubleLinkedList<Integer>();
		for(int i= 0; i < frecuencia.length; i++)
		{
			 aux.addLast(pQ.delMax());
		}
		
		// Merge a la lista de Rutas
		routes = routes.mergeSort(routes, new Comparator<RutasVO>() {
			public int compare(RutasVO arg0, RutasVO arg1) {return arg1.getCantidad() - arg0.getCantidad();}});
		
		
		
		Iterator<RutasVO> iterUno = routes.iterator();
		int a = 0;
		
		while(iterUno.hasNext())
		{
			RutasVO ruta = routes.getElement(a);
			
			// Iterador lista Auxiliar
			Iterator<Integer> iterDos = aux.iterator();
			int b = 0;
			
			while(iterDos.hasNext())
			{
				Integer intt = aux.getElement(b);
				
				if(ruta.getCantidad() == intt)
				{
					retorno.addLast(ruta);
				}
				
				// Avance Segunda Lista
				iterDos.next();
				b++;
			}
			// Avances Primera Lista
			iterUno.next();
			a++;
		}
		return retorno;
	}
}