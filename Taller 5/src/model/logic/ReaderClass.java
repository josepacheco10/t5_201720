package model.logic;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import model.data_structures.DoubleLinkedList;
import model.data_structures.IList;
import model.vo.TripsVO;

public class ReaderClass {

	//---------------------------------------
	// Constantes: (Ubicación de los archivos 
	//---------------------------------------
	
	public static final String tripsRead = "data/trips.txt";
	public static final String SEPARATOR = ",";
	
	//---------------------------------------
	// Atributos
	//---------------------------------------
	
	/**
	 * Atributo que representa la estructura de una Lista.
	 */
	public IList<TripsVO> trips;
	
	//---------------------------------------
	// Constructor
	//---------------------------------------
	
	public ReaderClass()
	{
		trips = new DoubleLinkedList<TripsVO>();
	}
	
	//---------------------------------------
	// Métodos
	//---------------------------------------
	/**
	 * Método encargado de realizar la lectura de un archivo CSV con la informacion 
	 * de los viajes.
	 */
	  public IList<TripsVO> loadTrips(String tripsFile) {

		  trips = new DoubleLinkedList<TripsVO>();
		  BufferedReader br = null;
			try {
				try 
				{
					br = new BufferedReader(new FileReader(tripsFile));
				} 
				catch (FileNotFoundException e) 
				{
					e.printStackTrace();
				}
				
				String line = br.readLine();
				line = br.readLine();
				while (null != line) {
					
					String[] fields = line.split(SEPARATOR);
					line = br.readLine();

					int route_id = Integer.parseInt(fields[0]);
					int servicios = Integer.parseInt(fields[1]);
					int tripId = Integer.parseInt(fields[2]);
					String tHead = fields[3];
					String tSN = fields[4];
					int directionId = Integer.parseInt(fields[5]);
					int blockId = Integer.parseInt(fields[6]);
					int shapedId = Integer.parseInt(fields[7]);
					int wcA = Integer.parseInt(fields[8]);
					int bA = Integer.parseInt(fields[9]);
					
					TripsVO nueva = new TripsVO(route_id, servicios, tripId, tHead, 
							tSN, directionId , blockId, shapedId, wcA, bA);
					
					trips.addLast(nueva);
				}
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (null != br) {
					try {
						br.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
			return trips;
		}
	  
	  
	  
	  
	  
	  
}