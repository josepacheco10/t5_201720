package model.data_structures;

public class Node<T>{

	/**
	 * Nodo sucesor.
	 */
	public Node<T> next;
	
	/**
	 * Nodo anterior,
	 */
	public Node<T> previous;
	
	/**
	 * Elemento asociado al Nodo.
	 */
	public T item;

	public Node(Node<T> n,Node<T> k, T pItem)
	{
		next = n;
		previous = k;
		item = pItem;
	}
	

	public Node<T> getNext()
	{
		return next;
	}
	
	public T darItem ()
	{
		return item;
	}
	
	public void cambiarElemento(T pItem)
	{
		item = pItem;
	}
	
	////////////////////////////////////////////////////
	//				  MODIFICADORES					////
	////////////////////////////////////////////////////
	
	public void setNext(Node<T> newN)
	{
		next = newN;
	}
	
	public void setPrevious(Node<T> newN)
	{
		previous = newN;
	}
}