package model.data_structures;

import java.util.Comparator;
import java.util.Iterator;

import Sorting.MergeSort;

public class DoubleLinkedList<T> implements IList<T> {

	//------------------------
	//	Llamado a la clase Merge
	//-----------------------
	/**
	 * Atributo que invoca la clase Merge
	 */
	private MergeSort merge;

	/**
	 * Cabeza de la lista, primer elemento.
	 */
	private Node<T> first;

	/**
	 * Ultimo elemento de la lista.
	 */
	private Node<T> last;

	/**
	 * Elemento actual en la lista.
	 */
	private Node<T> actual;

	/**
	 * Elemento sucesor en la lista.
	 */
	private Node<T> sig;

	/**
	 * Elemento sucesor en la lista.
	 */
	private Node<T> prev;

	/**
	 * Tamanio de la lista, cantidad de nodos.
	 */
	private Integer size;


	public DoubleLinkedList() {
		
		first = null;
		last = null;
		actual = null;
		sig = null;
		prev = null;
		size = 0;
	}


	/**
	 * Retorna el tamanio de la lista.
	 * @return Tamanio de la lista.
	 */
	public Integer getSize() {
		return size;
	}

	/**
	 * Informa si la lista se encuentra sin elementos.
	 * @return True si la lista esta vacia || False de lo contrario.
	 */
	public boolean isEmpty() {
		boolean iE = false;

		if (first == null) {
			iE = true;
		}
		return iE;
	}

	/**
	 * Retorna la cabeza de la lista, el primer elemento.
	 * @return Primer elemento en toda la lista.
	 */
	public Node<T> getFirst() {
		return isEmpty() ? null : first;
	}

	/**
	 * Retorna el �ltimo elemento de la lista.
	 * @return Ultimo elemento en toda la lista.
	 */
	public Node<T> getLast() {
		return isEmpty() ? null : last;
	}

	public void add(T nElement) {

		Node<T> nuevo = new Node<T>(null, null, nElement);

		if (isEmpty()) {
			first = nuevo;
			first.setPrevious(null);
			first.setNext(null);
			size++;
		} else 
		{
			first.setPrevious(nuevo);
			nuevo.setNext(first);
			first = nuevo;
			size++;
		}

	}

	@SuppressWarnings("unchecked")
	public void addAtk(T nElement, int pos) throws Exception {

		Integer num = 0;
		actual = first;

		Node<T> nuevo = new Node<T>(null, null, nElement);

		if (isEmpty()) {
			first = nuevo;
			nuevo.setNext(null);
			nuevo.setPrevious(null);
			size++;
		}

		else {
			if (pos == 0) {
				add(nElement);
			}

			else if (pos == size) {
				addLast(nElement);
			}

			else {
				while (actual != null) {

					actual = next();
					num++;

					if (num == pos) {
						if (actual != nElement) {
							// next();
							// previous();
							prev.setNext((Node<T>) nElement);
							actual.setPrevious((Node<T>) nElement);
							((Node<T>) nElement).setNext(actual);
							size++;
						} else {
							throw new Exception(
									"Ya existe un elemento igual al que desea agregar");
						}
					} else {
						throw new Exception("No existe la posicion deseada");
					}
				}
			}
		}

	}

	/**
	 * Agrega un elemento al final de la lista.
	 */
	public void addLast(T nElement) {

		Node<T> nuevo = new Node<T>(null, null, nElement);

		if (isEmpty()) {
			
			first = nuevo;
			last = nuevo;
			first.setPrevious(null);
			first.setNext(null);

			size++;
		} 
		
		else {
			nuevo.setPrevious(last);
			last.setNext(nuevo);
			last = nuevo;

			size++;
		}
	}

	public void delete(Node<T> nElement) throws Exception {

		actual = first;

		if (!isEmpty()) {

			if (nElement == first) {
				next();
				sig.setPrevious(null);
				first = sig;

				size--;
			}

			else if (nElement == last) {
				actual = last;
				previous();
				prev.setNext(null);
				last = prev;

				size--;
			} else {
				actual = first;

				next();
				previous();

				actual = first;
				next();
				previous();

				while (actual != null) {
					if (actual == nElement) {
						prev.setNext(sig);
						sig.setPrevious(prev);

						size--;
					}

					else {
						throw new Exception(
								"El elemento que se busca para eliminar, no existe actualmente en la lista.");
					}
				}
			}
		} else {
			throw new Exception("La lista se encuentra vacia");
		}

	}

	public void deleteAtk(Node<T> nElement, int pos) throws Exception {

		int num = 0;
		actual = first;

		if (pos == 0)
			delete(nElement);
		else if (pos == size)
			delete(nElement);

		else {
			while (actual != null) {
				if (num == pos) {
					if (actual == nElement) {
						sig.setPrevious(prev);
						prev.setNext(sig);
						size--;
					} else {
						throw new Exception(
								"No existe el elemento que desea eliminar");
					}
				}

				else {
					throw new Exception(
							"No existe el elemento que desea eliminar");
				}

				actual = sig;
				num++;

			}
		}
	}

	/**
	 * Obtiene el elemento de un Nodo dado, especificado a traves de un indice.
	 */
	public T getElement(Integer indice) {

		Node<T> respuesta = null;

		if (indice == 0)
			respuesta = first;

		else if (indice > 0) {
			int posN = indice;
			int contador = 0;
			Node<T> aux = first;

			while (aux != null) {
				if (contador == posN) {
					respuesta = aux;
					break;
				}
				aux = aux.next;
				contador++;
			}
		}
		return respuesta.item;
	}
	
	public Node<T> getNode(Integer indice)
	{
		Node<T> respuesta = null;
		
		if (indice == 0)
			respuesta = first;

		else if (indice > 0) {
			int posN = indice;
			int contador = 0;
			Node<T> aux = first;

			while (aux != null) {
				if (contador == posN) {
					respuesta = aux;
					break;
				}
				aux = aux.next;
				contador++;
			}
		}
		
		return respuesta;

	}
	
	
	public Node<T> darMitad()
	{
		return getNode((size/2)-1);
	}
	

	/**
	 * Retorna el elemento correspondiente al Nodo actual en la Lista.
	 */
	public T getCurrentElement() {
		return actual.item;
	}

	/**
	 * Retorna el nodo siguiente en la Lista.
	 */
	public Node<T> next() {
		return actual.getNext();
	}

	/**
	 * Retorna el nodo anterior en la Lista.
	 */
	public Node<T> previous() {
		return actual.previous;
	}
	
	
	//--------------------------------------------
	//	Ordenamiento
	//--------------------------------------------
	
	@SuppressWarnings("static-access")
	public DoubleLinkedList<T> mergeSort(IList<T> lista, Comparator <T> compa)
	{
		return merge.mergeSort((DoubleLinkedList<T>)lista, compa);
	}

	
	// -------------------------------------
	// Iterador.
	// -------------------------------------

	public Iterator<T> iterator() {
		return new DoubleLinkedListIterator();
	}

	private class DoubleLinkedListIterator implements Iterator<T> {

		Node<T> actual = first;

		public boolean hasNext() {
			return actual != null;
		}

		public T next() {
			T item = actual.item;
			actual = actual.next;
			return item;
		}

		public void remove() {
			throw new UnsupportedOperationException();

		}
	}
}