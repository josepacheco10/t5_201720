package model.vo;

public class TripsVO {

	private int routeId;
	private int serviceId;
	private int tripId;
	private String tripHeadSing;
	private String tripShortName;
	private int directionId;
	private int blockId;
	private int shapeId;
	private int wheelchairAccessible;
	private int bikesAllowed;

	public TripsVO (int pRouteId, int pServiceId, int pTripId,
			String pTripHeadSing, String pTripShortName, int pDirectionId,
			int pBlockId, int pShapeId, int pWheelchairAccessible,
			int pBikesAllowed) 
	{
		routeId = pRouteId;
		serviceId = pServiceId;
		tripId = pTripId;
		tripHeadSing = pTripHeadSing;
		tripShortName = pTripShortName;
		directionId = pDirectionId;
		blockId = pBlockId;
		shapeId = pShapeId;
		wheelchairAccessible = pWheelchairAccessible;
		bikesAllowed = pBikesAllowed;
	}

	public int getRouteId() {
		return routeId;
	}

	public int getServiceId() {
		return serviceId;
	}

	public int getTripId() {
		return tripId;
	}

	public String getTripHeadSign() {
		return tripHeadSing;
	}

	public String getTripShortName() {
		return tripShortName;
	}

	public int getDirectionId() {
		return directionId;
	}

	public int getBlockId() {
		return blockId;
	}

	public int getShapeId() {
		return shapeId;
	}

	public int getWheelChairAccessible() {
		return wheelchairAccessible;
	}

	public int getBikesAllowed() {
		return bikesAllowed;
	}	
}