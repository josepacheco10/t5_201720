package model.vo;

import model.data_structures.IList;

public class ResumenList {

	// -------------------------------
	// Atributos
	// -------------------------------

	private int blockID;
	private int routeID;
	private int totalBlockID;
	private IList<Integer> tripsID;

	// -------------------------------
	// Constructor
	// -------------------------------

	public ResumenList(int pBlock, int pRoute, int pTotalBlock,IList<Integer> pTripsID) {
		blockID = pBlock;
		routeID = pRoute;
		totalBlockID = pTotalBlock;
		tripsID = pTripsID;
	}

	// -------------------------------
	// M�todos
	// -------------------------------
	public int getBlockID() {
		return blockID;
	}

	public int getRouteID() {
		return routeID;
	}

	public int getTotalBlockID() {
		return totalBlockID;
	}

	public IList<Integer> getTripsID() {
		return tripsID;
	}
}