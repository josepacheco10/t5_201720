package model.vo;

public class RutasVO {

	// -------------------------------------
	// Atributos.
	// -------------------------------------
	/**
	 * Atributo que indica el identificador de la ruta.
	 */
	private int routeID;
	
	/**
	 * Atributo para la cantidad de veces que aparece en lista.
	 */
	private int cantidad;
	
	
	// -------------------------------------
	// Constructor.
	// -------------------------------------
	public RutasVO(int pId, int pContador)
	{
		routeID = pId;
		cantidad = pContador;
	}

	
	// -------------------------------------
	// M�todos.
	// -------------------------------------
	/**
	 * Obtener el identificador de ruta.
	 * @return int. Id de la ruta
	 */
	public int getRouteID() {
		return routeID;
	}

	/**
	 * Obtener la cantidad de veces que un ID aparece en lista.
	 * @return int. Cantidad de veces en Lista.
	 */
	public int getCantidad() {
		return cantidad;
	}
	
	
	
}
