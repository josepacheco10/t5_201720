package api;

import model.data_structures.IList;
import model.vo.RutasVO;

public interface ISTSManager {

	/**
	 * M�todo encargado de realizar la lectura de un archivo CSV con la
	 * informacion de los viajes.
	 */
	public void loadTrips();
	
	/**
	 * M�todo encargado de ordenar la Lista de viajes por el par�metro: BlockId.
	 */
	public void ordenarTrips();
	
	/**
	 * A partir de la Lista de Viajes, obtiene un resumen con determinados par�metros.
	 */
	public void buildResumenList();
	
	/**
	 * Ordena la Lista de identificadores de viaje a trav�s del algoritmo HeapSort
	 */
	public void ordenarHeapSort();
	
	/**
	 * M�todo para obtener los identificadores de Ruta y su frecuencia en la lista resumen.
	 * @return IList. Lista de tipo RutasVO --> Atributos: RouteID, Frecuencia(cantidad).
	 */
	public IList<RutasVO> obtenerRutas();
	
	/**
	 * M�todo para agregar las rutas a una Priority Queue, a partir de esta se saca el m�ximo
	 * valor y se agrega a una lista.
	 * @return IList. Lista con las Rutas y su prioridad ordenadas. Mayor a menor.
	 */
	public IList<RutasVO> listasPQ();
	
}
