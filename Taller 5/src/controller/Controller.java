package controller;

import model.data_structures.IList;
import model.logic.STSManager;
import model.vo.RutasVO;
import api.ISTSManager;

public class Controller {

	//-------------------------------------
	// Atributos.
	//-------------------------------------
	/**
	 * Reference to the routes and stops manager
	 */
	private static ISTSManager  manager = new STSManager();

	//-------------------------------------
	// Metodos.
	//-------------------------------------
	
	/**
	 * M�todo encargado de realizar la lectura de un archivo CSV con la
	 * informacion de los viajes.
	 */
	public static void loadTrips()
	{
		manager.loadTrips();
	}
	
	/**
	 * M�todo encargado de ordenar la Lista de viajes por el par�metro: BlockId.
	 */
	public static void ordenarTrips()
	{
		manager.ordenarTrips();
	}
	
	/**
	 * A partir de la Lista de Viajes, obtiene un resumen con determinados par�metros.
	 */
	public static void buildOtherList()
	{
		manager.buildResumenList();
	}
	
	/**
	 * Ordena la Lista de identificadores de viaje a trav�s del algoritmo HeapSort
	 */
	public static void ordenarHeapSort()
	{
		manager.ordenarHeapSort();
	}
	
	/**
	 * M�todo para obtener los identificadores de Ruta y su frecuencia en la lista resumen.
	 * @return IList. Lista de tipo RutasVO --> Atributos: RouteID, Frecuencia(cantidad).
	 */
	public static IList<RutasVO> obtenerRutas()
	{
		return manager.obtenerRutas();
	}
	
	/**
	 * M�todo para agregar las rutas a una Priority Queue, a partir de esta se saca el m�ximo
	 * valor y se agrega a una lista.
	 * @return IList. Lista con las Rutas y su prioridad ordenadas. Mayor a menor.
	 */
	public static IList<RutasVO> listasPQ()
	{
		return manager.listasPQ();
	}
	
}