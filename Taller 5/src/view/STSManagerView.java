package view;

import java.util.Scanner;

import model.data_structures.IList;
import model.vo.RutasVO;
import controller.Controller;

public class STSManagerView {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		boolean fin = false;
		while (!fin) {
			printMenu();

			int option = sc.nextInt();

			switch (option) {

			// Carga los archivos estáticos
			case 1:

				// Memoria y tiempo
				long memoryBeforeCase1 = Runtime.getRuntime().totalMemory()- Runtime.getRuntime().freeMemory();
				long startTime = System.nanoTime();

				// Cargar data
				Controller.loadTrips();

				// Tiempo en cargar
				long endTime = System.nanoTime();
				long duration = (endTime - startTime) / (1000000);

				// Memoria usada
				long memoryAfterCase1 = Runtime.getRuntime().totalMemory()- Runtime.getRuntime().freeMemory();
				System.out.println("Tiempo en cargar: " + duration + " milisegundos \nMemoria utilizada:  " + ((memoryAfterCase1 - memoryBeforeCase1) / 1000000.0) + " MB");

				break;

			case 2:
		
				// Ordenar la Lista de Trips (MergeSort)
				Controller.ordenarTrips();
				
				break;
				
			case 3:
				// Crea la Lista Resumen, este método puede tardar un poco.
				Controller.buildOtherList();
				break;
				
			case 4:
				// Ordenar la lista con lista de identificadores de viaje (TripsID) con HeapSort.
				Controller.ordenarHeapSort();
				break;
				
			case 5:
				//Controller.obtenerRutas();
				IList<RutasVO> listaCase = Controller.obtenerRutas();
				
				if(listaCase.isEmpty())
				{
					System.out.println("No sirve");
				}
				
				else
				{
					for(RutasVO rutaCase: listaCase)
					{
						System.out.println(rutaCase.getRouteID() + " Frecuencia: " + rutaCase.getCantidad() );
					}
				}
				
				break;
			
			case 6:
				
				IList<RutasVO> listaCaseDos = Controller.listasPQ();
				
				if(listaCaseDos.isEmpty())
				{
					System.out.println("No sirve");
				}
				
				else
				{
					for(RutasVO rutaCase: listaCaseDos)
					{
						System.out.println(rutaCase.getRouteID() + " Frecuencia: " + rutaCase.getCantidad() );
					}
				}

				break;
				
			case 7:
				fin = true;
				sc.close();
				break;
			}
		}
	}
	

	private static void printMenu() {

		System.out.println("--------------------------- ISIS 1206 - Estructuras de datos--------------------------------------------");
		System.out.println("------------------------------------ Taler 4 - 5 --------------------------------------------------");
		System.out.println("1. Cargar la información de viajes a partir del archivo trips.txt");
		System.out.println("2. Ordenar la lista usando el algoritmo de ordenamiento: MergeSort. Parámetro usado: BlockID");
		System.out.println("3. Crear una lista alterna (resumen) a partir de determinados atributos de la lista de viajes.");
		System.out.println("4. Ordenar la lista usando el algoritmo de ordenamiento: HeapSort. Parámetro usado: TripID");
		System.out.println("5. Obtener una lista con las rutas (ID's y frecuencias) (Nota: No es el punto 4)");
		System.out.println("6. Obtener una lista con las rutas (ID's y frecuencias) haciendo uso de una cola de Prioridad (pto.4)");
		System.out.println("7. Salir.");
	}

}