package Sorting;

import java.util.Comparator;

import model.data_structures.DoubleLinkedList;
import model.data_structures.IList;
import model.data_structures.Node;


public class MergeSort {
	
	public static<T> void cambiar(IList<T> lista, Integer x, Integer y)
	{
		Node<T> nodo1 = lista.getNode(x);
		Node<T> nodo2 = lista.getNode(y);
	
		T ob1 = nodo1.item;
		T ob2 = nodo2.item;
		
		nodo1.cambiarElemento(ob2);
		nodo2.cambiarElemento(ob1);
	}
	
	
	public static <T> DoubleLinkedList<T> mergeSort(DoubleLinkedList<T> lista, Comparator<T> comp) 
	{
		if(lista.getFirst() == null||lista.getFirst().next == null)
			return lista;

		DoubleLinkedList<T> izquierda = new DoubleLinkedList<T>();
		DoubleLinkedList<T> derecha = new DoubleLinkedList<T>();

		Node<T>mitad = lista.darMitad();
		Node<T> actual1 = lista.getFirst();
		Node<T> actual2 = mitad.next;
		
		while(actual1 != mitad.next)
		{
			izquierda.addLast(actual1.item);
			
			actual1 = actual1.next;
		}
		
		while(actual2!=null)
		{
			derecha.addLast(actual2.item);
			
			actual2 = actual2.next;
		}
		
		return merge(mergeSort(izquierda, comp), mergeSort(derecha, comp), comp);
	}

	
	public static<T> DoubleLinkedList<T> merge(DoubleLinkedList<T> izquierda, DoubleLinkedList<T> derecha, Comparator<T> comp) 
	{
		DoubleLinkedList<T> retornable= new DoubleLinkedList<T>();
		
		Node<T> nI = izquierda.getFirst();
		
		Node<T> nD = derecha.getFirst();
		
		while(nI!=null&&nD!=null)
		{		
			T pI = nI.item;
			T pD = nD.item;
			
			if(comp.compare(pI, pD)<=0)
			{
				retornable.addLast(pI);
				nI=nI.next;
			}
			else
			{
				retornable.addLast(pD);
				nD = nD.next;
			}
		}
		
		if(nI==null&&nD!=null)
		{
			while(nD!=null)
			{
				retornable.addLast(nD.item);
				
				nD = nD.next;
			}
		}
		
		else if(nI!=null&&nD==null)
		{
			while(nI!=null)
			{
				retornable.addLast(nI.item);
				nI=nI.next;
			}
		}
		return retornable;
	}

}
