package Sorting;

public class HeapSort
{
	public void sort(int arr[])
    {
        int size = arr.length;
        
        for (int i = size / 2 - 1; i >= 0; i--)
            heapify(arr, size, i);
 
        for (int i= size-1; i>=0; i--)
        {
            int temp = arr[0];
            arr[0] = arr[i];
            arr[i] = temp;

            heapify(arr, i, 0);
        }
    }
 
    void heapify(int arr[], int size, int indice)
    {
        int max = indice; 
        int izq = 2*indice + 1;
        int der = 2*indice + 2;  
 
        if (izq < size && arr[izq] > arr[max]) max = izq;
 
        if (der < size && arr[der] > arr[max]) max = der;
 
        if (max != indice)
        {
            int swap = arr[indice];
            arr[indice] = arr[max];
            arr[max] = swap;
 
            heapify(arr, size, max);
        }
    }
}